import React, { Component } from 'react';
import Content from './components/Content/Content';
import Header from './components/Header/Header';
import createBrowserHistory from 'history/createBrowserHistory';

import { Router } from "react-router-dom";

const history = createBrowserHistory();

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <div>
          <Header />
          <Content />
        </div>
      </Router>
    );
  }
}

export default App;

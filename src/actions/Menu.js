import request from 'superagent'

import { PARCEL_URL } from '../constants/api'

export const menuHead = (data) => { 
    request
    .get(`${PARCEL_URL}/menu.json`)
    .then(res => {
        return res.body;
    })
    .catch(err => {
        return err;
        // err.message, err.response
    });
}
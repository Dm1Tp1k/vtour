import React from 'react';
import "./Button.scss";

export default function Button(props) {
    return (
        <>
            <button style={props.style}>{props.title}</button>
        </>
    )
}
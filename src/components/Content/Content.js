import React from 'react'
import { Route } from 'react-router-dom'
import Home from '../../pages/Home';

// const PrivateRoute = ({ needRedirect, redirectTo, component: Component, ...rest }) => (
//     <Route
//         {...rest}
//         render={props =>
//             (needRedirect ? (
//                 <Component {...props}>
//                     {rest.children}
//                 </Component>
//             ) : (
//                 <Redirect
//                     to={{ pathname: redirectTo }}
//                 />
//             ))
//         }
//     />
// );

const Content = () => {
    return (
        <div>
            <Route
                exact path='/'
                component={Home}
            />
            <Route
                exact path='/test'
                component={Home}
            />
        </div>
    )
}

export default Content
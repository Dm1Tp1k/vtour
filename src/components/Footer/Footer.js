import React from 'react';
import "./Footer.scss";
import Container from '../Container/Container';
import logo from '../../assets/img/logo.svg';
import { Link } from "react-router-dom";



export default function Footer() {

    return (
        <div className="footer">
            <Container>
                <div className="footer_row">
                    <div className="logo">  
                        <Link to="/"><img src={logo} alt="logo" /></Link>
                    </div>
                    <div className="footer_contacts">
                        <a href="tel:941.681.8222">941.681.8222</a> • <a href="mailto:team@vtourstudios.com">team@vtourstudios.com</a>
                    </div>
                    <div className="footer_copyright">
                        Copyright © 2019 vTour Studios. All Rights Reserved. <br />
                        developed by StoryExpress
                    </div>
                </div>
            </Container>
        </div>
    )
}
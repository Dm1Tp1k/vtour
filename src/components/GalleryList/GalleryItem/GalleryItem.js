
import React from 'react';
import './GalleryItem.scss';
import Button from '../../Button/Button';

export default function GalleryItem(props) {
    const chunks = props.gallery.reduce((chunks, value, index) => {
        const chunkIndex = Math.floor(index / 3);
        if (!chunks[chunkIndex]) {
        chunks[chunkIndex] = [];
        }
        chunks[chunkIndex].push(value);
        return chunks;
    }, []);

    return (
        <div className="tabs-item">
            <div className="tabs-button">
                <Button style={{backgroundColor: "#a1d3de", marginRight: "30px", padding: "22px 42px"}} title="Slideshow" />
                <Button style={{padding: "22px 42px"}} title="Download Photos" />
            </div>
            {chunks.map((chunk, index) => (
                <div className={(index % 2 === 0) ? "gallery gallery--left" : "gallery gallery--right"} key={index}>
                    
                    <div className="gallery__main">
                        <img src={chunk[0].url} alt={chunk[0].alt}/>
                    </div>
                    <div className="gallery__small">
                        <img src={chunk[1].url} alt={chunk[1].alt}/>
                        <img src={chunk[2].url} alt={chunk[2].alt}/>
                    </div>
                </div>
            ))}
        </div>
    )

}
    

  

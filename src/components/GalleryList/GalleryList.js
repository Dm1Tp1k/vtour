import React, { useState, useEffect } from 'react';
import Container from '../Container/Container';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import GalleryItem from './GalleryItem/GalleryItem';
import request from 'superagent'
import { PARCEL_URL } from '../../constants/api'
import "./GalleryList.scss";

const tabStyle = {
    default_tab: {
        color: '#555555',
        fontSize: 14,
        textTransform: 'none',
    },
    active_tab:{ 
        color: '#3C9CAB',
        fontSize: 14,
        textTransform: 'none',
    }
};

let didCancel = false;

export default function GalleryList(props) {
    const [tabs, setTabs] = useState(0);
    const [items, setItems] = useState([]);

    function handleTabChange (event, index) {
        setTabs(index)
    }

    function getStyle (isActive) {
        return isActive ? tabStyle.active_tab : tabStyle.default_tab
    }

    useEffect(() => {
        if (!didCancel) {        
            const getGallery = async () => {
                try {
                    const result = await request.get(`${PARCEL_URL}/galleryItem.json`);
                    setItems(result.body);
                    return result.body;
                } catch (error) {
                }
            }
            getGallery()
        }
        return () => {
            didCancel = true;
        };
    }, [items]);
    
    return (
        <Container className="galleryList">
            <Tabs 
                value={tabs} 
                centered 
                onChange={handleTabChange}
                className="tabs"
                TabIndicatorProps={{
                    style: {
                        backgroundColor: "#3C9CAB",
                        height: "3px"
                    }
                }}
            >
            {items.map((item, index) => (
                <Tab style={getStyle(tabs === index)} label={item.title} key={index} />
            ))}
            </Tabs>
            {items.map((item, index) => (
                tabs === index && <GalleryItem gallery={item.gallery} key={index} />
            ))}
        </Container>
    )
}
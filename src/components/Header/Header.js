
import React from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import Menu from '../Menu/Menu';
import { Link } from "react-router-dom";
import { createStyles } from '@material-ui/styles';
import withStyles from "@material-ui/styles/withStyles";
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';

import './Header.scss';
import logo from '../../assets/img/logo.svg';

const styles = createStyles({
    hamburger: {
        border: 0,
        borderRadius: 0,
        color: 'white',
        height: 68,
        width: 68,
        marginLeft: '30px',
        backgroundColor: '#168CA8',
    },
    icon: {
        fontSize: '34px',
        borderRadius: 0
    }
});

function Header() {
    return (
        <div className="header">
            <Grid container wrap="nowrap">
                <div className="logo">  
                    <Link to="/"><img src={logo} alt="logo" /></Link>
                </div>
                <Grid item xs={12} className="header_menu">
                    <Menu />
                    <Fab color="inherit" aria-label="Menu" style={styles.hamburger}>
                        <MenuIcon style={styles.icon} />
                    </Fab>
                </Grid>
            </Grid>
        </div>
    );
}

  
export default withStyles(styles)(Header);
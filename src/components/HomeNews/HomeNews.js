
import React, { Component } from 'react';
import './HomeNews.scss';
import Container from '../Container/Container';
import homeNews from '../../assets/img/home_news.jpg';
import Button from '../Button/Button';

class HomeNews extends Component {
    render() {
      return (
        <Container className="homeNews">
          <h2>Bradenton</h2>
          <div className="homeNews__row">
            <div className="homeNews__text">
              <p>Bradenton /ˈbreɪdəntən/ BRAY-den-ton is a city in Manatee County, Florida, United States. The U.S. Census Bureau estimated the city's 2016 population to be 54,437.[4] Bradenton is a principal city of the Bradenton-Sarasota-Venice, Florida Metropolitan Statistical Area, which had a 2007 estimated population of 682,833.[8] It is the county seat.</p>
              <Button style={{marginTop: "28px"}} title="More Info"/>
            </div>
            <div className="homeNews__img">
              <img src={homeNews} alt="" />
            </div>
          </div>
        </Container>
      )
    }
  }
  
export default HomeNews;

import React from 'react';
import './HomeSlider.scss';
import HomeSliderMenu from '../HomeSliderMenu/HomeSliderMenu';
    
function HomeSlider() {
  return (
    <div className="HeaderSlider">
      <div>
          <div className="HeaderSlider_text">
              10224 46th Ave W 
              <div className="HeaderSlider_text--small">Bradenton, FL</div>
          </div>
      </div>
      <HomeSliderMenu />
    </div>
  );
}
  
export default HomeSlider;
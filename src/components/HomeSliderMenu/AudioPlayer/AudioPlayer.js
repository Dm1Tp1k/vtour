import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import './AudioPlayer.scss';
import IconButton from '@material-ui/core/IconButton';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseCircleFilled from '@material-ui/icons/PauseCircleFilled';
import Shuffle from '@material-ui/icons/Shuffle';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = theme => ({
    linearColorPrimary: {
      backgroundColor: 'transparent',
    },
    linearBarColorPrimary: {
      backgroundColor: 'rgba(255, 255, 255, 0.8)',
    },
});
  

function AudioPlayer(props) {
    const [play, setPlay] = useState(false);
    const { classes } = props;

    function handlePlay() {
        setPlay(play ? false : true);
    }

    return (
        <div className="audioPlayer">
            <div className="audioPlayer_main">
                <IconButton aria-label="Previous">
                    <SkipPreviousIcon />
                </IconButton>
                <IconButton aria-label="Play/pause" className="control" onClick={handlePlay}>
                    { play ? <PauseCircleFilled /> : <PlayArrowIcon /> }
                </IconButton>
                <IconButton aria-label="Next">
                    <SkipNextIcon />
                </IconButton>
            </div>
            <div className="audioPlayer_footer">
                <div className="audioPlayer_info">
                    <div>1/5</div>
                    <IconButton>
                        <Shuffle />
                    </IconButton>
                </div>
                <LinearProgress 
                    variant="determinate" 
                    className="progressBar" 
                    value={50} 
                    classes={{
                        colorPrimary: classes.linearColorPrimary,
                        barColorPrimary: classes.linearBarColorPrimary,
                      }}
                />
            </div>  
        </div>
    )
}

AudioPlayer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AudioPlayer);

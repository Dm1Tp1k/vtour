import React from 'react';
import './Details.scss';

export default function Details(props) {
    return (
        <div className="details">
            <div className="billet_title" dangerouslySetInnerHTML={ { __html: props.title } } />
            <div className="billet_description" dangerouslySetInnerHTML={ { __html: props.description } } />
        </div>
    )

}
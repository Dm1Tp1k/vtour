import React, { useState, useEffect } from 'react';
import './HeaderBillet.scss';

export default function HeaderBillet(props) {
    return (
        <div className="billet">
            <div className="billet_inner">
                <div className="billet_title" dangerouslySetInnerHTML={ { __html: props.content.title } } />
                <div className="billet_description" dangerouslySetInnerHTML={ { __html: props.content.description } } />
            </div>
        </div>
    )

}
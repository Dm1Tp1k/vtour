import React, { useState } from 'react';
import './HomeSliderMenu.scss';
import SearchForm from './SearchForm/SearchForm';
import HeaderBillet from './HeaderBillet/HeaderBillet';
import AudioPlayer from './AudioPlayer/AudioPlayer';
import Realtor from './Realtor/Realtor';
import Shortcuts from './Shortcuts/Shortcuts';
import Details from './Details/Details';

export default function HomeSliderMenu(props) {
    const [test, setTest] = useState(false);

    function handleState(value) {
        var elems = document.querySelectorAll(".HomeSliderMenu_Item.open");
        [].forEach.call(elems, function(el) {
            el.classList.remove("open");
        });
        
        value.target.closest('.HomeSliderMenu_Item').classList.add("open");
    }

    return (
        <div className="HomeSliderMenu" style={{marginTop: "-140px"}}>
            <div className="HomeSliderMenu_Item" style={{backgroundColor: "#5bb8ce"}} >
                <AudioPlayer />
            </div>

            <div className="HomeSliderMenu_Item" style={{backgroundColor: "#319cb5"}} onClick={handleState} >
                <HeaderBillet 
                    content={{
                        "title": "Realtor",
                        "description": "<p>Gail Tutewiler</p><p>941-705-0227</p><p>GailTuteRE@aol.com</p><p>www.IslandGail.com</p>"
                    }}
                   
                />
                <Realtor 
                    content={
                        {
                          "title": "Photo Tour",
                          "description": {
                              "name": "Gail Tutewiler",
                              "phone": "941-705-0227",
                              "email": "GailTuteRE@aol.com",
                              "site": "www.IslandGail.com"
                          },
                          "avatar": "http://localhost:3000/api/img/realtor.jpg"
                        }
                      }
                />
            </div>

            <div className="HomeSliderMenu_Item" style={{backgroundColor: "#5bb8ce"}} onClick={handleState} >
                <HeaderBillet 
                    content={{
                        "title": "Shortcuts",
                        "description": "<p>Print Flyer</p><p>Download Photos</p><p>Download Video</p><p>Embed VTour</p>"
                    }}
                />
                <Shortcuts 
                    content={{
                        "title": "Shortcuts",
                        "items": [
                            { 
                                "title": "Print Flyer",
                                "link": "http://google.com"
                            },
                            { 
                                "title": "Download Photos",
                                "link": "http://google.com"
                            },
                            { 
                                "title": "Download Video",
                                "link": "http://google.com"
                            },
                            { 
                                "title": "Embed Vtour",
                                "link": "http://google.com"
                            },
                        ]
                    }}
                />
            </div>

            <div className="HomeSliderMenu_Item" style={{backgroundColor: "#319cb5"}} onClick={handleState} >
                <HeaderBillet 
                    content={{
                        "title": "Details",
                        "description": "<p>5 Bedrooms</p><p>4 Bathrooms</p><p>3638 Sq. Ft.</p><p>2 Car Garage</p>" 
                    }}
                />
                <Details />
            </div>
            <div className="HomeSliderMenu_Item" style={{backgroundColor: "#168CA8"}} >
                <SearchForm />
            </div>
        </div>
    )
  }
  

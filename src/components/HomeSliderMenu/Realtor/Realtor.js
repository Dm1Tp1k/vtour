import React from 'react';
import './Realtor.scss';

export default function Realtor(props) {
    console.log(props.content.title);
    return (
        <div className="realtor">
            <div className="realtor_inner">
                {props.content.avatar ? (
                    <div className="realtor_avatar">
                        <img src={props.content.avatar} alt="" />
                    </div>
                ) : ""}
                <div className="realtor_info"> 
                    <div className="realtor_info-name">{props.content.description.name}</div>
                    <div className="realtor_info-contacts">
                        <p><a href={"tel:"+(props.content.description.phone)} >{props.content.description.phone}</a></p>
                        <p><a href={"mailto:"+(props.content.description.email)} >{props.content.description.email}</a></p>
                    </div>
                    <div className="realtor_info-site">{props.content.description.site}</div>
                </div>
            </div>
        </div>
    )

}
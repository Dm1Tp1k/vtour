import React from 'react';
import Search from '@material-ui/icons/Search';
import './SearchForm.scss';

export default function SearchForm(props) {
    return (
        <div className="search">
            <Search />
        </div>
    )
  }
  

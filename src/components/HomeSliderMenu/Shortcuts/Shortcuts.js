import React from 'react';
import './Shortcuts.scss';

export default function Shortcuts(props) {
    return (
        <div className="shortcuts showHeaderMenu">
            <div className="shortcuts_inner">
                <div className="billet_title" dangerouslySetInnerHTML={ { __html: props.title } } />
                {props.content.items ? (
                    <ul>
                        {props.content.items.map(item => (
                            <li>
                                <a href={item.link}>{item.title}</a>
                            </li>
                        ))}
                       
                    </ul>
                ) : ''}
                
            </div>
        </div>
    )
}
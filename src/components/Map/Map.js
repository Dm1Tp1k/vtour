
import React from 'react';
import './Map.scss';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker
} from "react-google-maps";
import { compose, withProps } from "recompose";
import logo from '../../assets/img/logo.svg';


const Map = compose(
    withProps({
      googleMapURL:
        "https://maps.googleapis.com/maps/api/js?key=AIzaSyBUgzdhBjUAgvO8neToFtslXrMD6Ze4JY8&v=3.exp&libraries=geometry,drawing,places",
      loadingElement: <div style={{ height: `100%` }} />,
      containerElement: <div className="map-container"/>,
      mapElement: <div className="map-elem" />
    }),
    withScriptjs,
    withGoogleMap
)(props => (
    <GoogleMap 
        defaultZoom={8}     
        defaultOptions={{
            streetViewControl: false,
            scaleControl: false,
            mapTypeControl: false,
            panControl: false,
            zoomControl: false,
            rotateControl: false,
            fullscreenControl: false
        }} 
        defaultCenter={{ lat: -34.397, lng: 150.644 }}
    >
        <Marker position={{ lat: -34.397, lng: 150.644 }} icon={logo}/>
    </GoogleMap>
));
  
export default Map;
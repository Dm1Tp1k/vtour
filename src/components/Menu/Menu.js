
import React, { Component } from 'react';
import './Menu.scss';
import { Link } from "react-router-dom";
import request from 'superagent'
import { PARCEL_URL } from '../../constants/api'


class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: true,
            items: []
        };
    }
    componentDidMount() {
        request
            .get(`${PARCEL_URL}/menu.json`)
            .then(res => {
                this.setState({
                    isLoaded: true,
                    items: res.body
                });
            })
            .catch(err => {
                this.setState({
                    error: err
                });
            });
    }
    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
          return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
          return <div>Loading...</div>;
        } else {
          return (
            <div>
                <ul className="headerMenu">
                    {items.map(item => (
                        <li key={item.title}>
                            <Link to={item.url}>{item.title}</Link>
                        </li>
                    ))}
                </ul>
            </div>
          );
        }

    }
  }
  
export default Menu;
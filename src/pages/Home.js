
import React from 'react';
import HeaderSlider from '../components/HomeSlider/HomeSlider';
import GalleryList from '../components/GalleryList/GalleryList';
import Map from '../components/Map/Map';
import HomeNews from '../components/HomeNews/HomeNews';
import Footer from '../components/Footer/Footer';

function Home() {
  return (
    <>
      <HeaderSlider />
      <GalleryList />
      <Map />
      <HomeNews />
      <Footer />
    </>
  );
}
  
export default Home;
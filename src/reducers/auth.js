const initState = {
    login: false,
}

export default function (state = initState, action) {
    switch (action.type) {
        case 'AUTH_USER_SUCCESS': {
            return { ...state, login: true }
        }
        
        case 'AUTH_USER_LOGOUT': {
            return { ...state, login: false }
        }

        default:
            return state
    }
}
const initState = {
    text: '',
    link: '',
}

export default function (state = initState, action) {
    switch (action.type) {
        case 'GET_PARCELS': {
            return { ...state, parcels: action.parcels, trackParcels: [] }
        }

        default:
            return state
    }
}